package omen

import (
	"crypto/tls"
	"github.com/gorilla/websocket"
	"gitlab.com/Varjelus/cga-go"
	"log"
	"net"
	"net/http"
	"os"
	"sync"
)

var StdLogger *log.Logger = log.New(os.Stdout, "[OMEN]\t", log.LstdFlags)

type Config struct {
	Location    string
	Origin      string
	CheckOrigin func(*http.Request) bool
	Security    byte
	TLSConfig   *tls.Config
	Logger      *log.Logger

	ReadBufferSize, WriteBufferSize int
}

type Server struct {
	Config

	Channels     map[[]byte]*Channel
	channelMutex *sync.Mutex

	Clients     map[Fingerprint]chan []byte
	clientMutex *sync.Mutex
}

func New(cfg *Config) (http.Handler, error) {
	srv := &Server{*cfg, []byte{}}
	return srv, nil
}

func (srv *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Finish up the handshake.
	fingerprint, err := srv.handshake(w, r)
	if err != nil {
		panic(err)
	}

	// Upgrade HTTP TCP connection to WebSocket.
	conn, err := srv.upgrade(w, r)
	if err != nil {
		panic(err)
	}

	client := &webSocketClient{
		conn,
		fingerprint,
		[]*Channel{},
		make(chan []byte),
	}

	// Handle the connection.
	if err := srv.handle(client); err != nil {
		panic(err)
	}
}

func (srv *Server) handshake(w http.ResponseWriter, r *http.Request) (Fingerprint, error) {
	if srv.TLSConfig == nil {
		// TODO: what we wanna do in this case?
	}
	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return nil, err
	}
	// TODO: Find client certificate: r.TLS.?
	a, err := cga.Generate(net.ParseIP(host), srv.Security, []byte{1, 2, 3}, []byte{}, nil)
	if err != nil {
		return nil, err
	}
	return a.Address, nil
}

func (srv *Server) upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	return (&websocket.Upgrader{
		ReadBufferSize:  srv.ReadBufferSize,
		WriteBufferSize: srv.WriteBufferSize,
		Subprotocols:    []string{"omen"}, // Unregistered
		CheckOrigin:     srv.CheckOrigin,
	}).Upgrade(w, r, nil)
}

func (srv *Server) handle(client Client) error {
	if srv.Logger != nil {
		srv.Logger.Printf("{%v} connected from %s\n", fingerprint, conn.RemoteAddr().String())
	}

	for {
		msgType, msg, err := conn.ReadMessage()
		if err != nil {
			if srv.Logger != nil {
				srv.Logger.Printf("{%v} Read error: %v\n", fingerprint, err)
			}
			break
		}
		if srv.Logger != nil {
			srv.Logger.Printf("{%v} -> %v\n", fingerprint, msg)
		}
		if err := conn.WriteMessage(msgType, msg); err != nil {
			if srv.Logger != nil {
				srv.Logger.Printf("{%v} Write error: %v\n", fingerprint, err)
			}
			break
		}
		if srv.Logger != nil {
			srv.Logger.Printf("{%v} <- %v\n", fingerprint, msg)
		}
	}

	if srv.Logger != nil {
		srv.Logger.Printf("{%v} disconnected\n", fingerprint)
	}
	return nil
}
