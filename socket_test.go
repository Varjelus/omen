package omen

import (
	"crypto/tls"
	"golang.org/x/net/websocket"
	"net/http"
	"testing"
)

func TestHandshake(t *testing.T) {
	origin := `http://localhost`
	wsURI := `ws://localhost:8080`
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}

	cfg := &Config{
		Location:  wsURI,
		Origin:    origin,
		Security:  0x0,
		TLSConfig: tlsConfig,
	}

	srv, err := New(cfg)
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		http.Handle("/", srv)
		if err := http.ListenAndServe("localhost:8080", nil); err != nil {
			t.Fatal(err)
		}
	}()

	cli, err := websocket.Dial(wsURI, "omen", origin)
	if err != nil {
		t.Fatal(err)
	}

	if _, err := cli.Write([]byte("Message\n")); err != nil {
		t.Log(err)
	}

	var buf = make([]byte, 1024)
	if _, err := cli.Read(buf); err != nil {
		t.Log(err)
	}
}
