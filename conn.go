package omen

import (
	"errors"
	"github.com/gorilla/websocket"
	"gitlab.com/Varjelus/jose"
	"net"
	"time"
)

var (
	ErrSendTimeout  = errors.New("client send timed out")
	ErrSocketClosed = errors.New("socket is closed")
)

// Conn extends net.Conn interface.
type Conn interface {
	net.Conn
	// Whatever Conn.Read produces MUST be valid JSON encoded JOSE JWE object.
	// Whatever is written to Conn.Write MUST be valid JSON encoded JOSE JWE object.
	State() State
	Keys() *jose.KeySet
}

// Follows WebSocket readyState specification.
type State int

const (
	CONNECTING State = 0
	OPEN       State = 1
	CLOSING    State = 2
	CLOSED     State = 3
)

type webSocketClient struct {
	connection *websocket.Conn
	id         Fingerprint
	channels   []*Channel
	input      chan []byte
}

func (wsc *webSocketClient) Id() Fingerprint {
	return wsc.id
}

func (wsc *webSocketClient) Send(b []byte, d time.Duration) error {
	timeout := time.NewTicker(d)
	select {
	case wsc.input <- b:
		timeout.Stop()
		return nil
	case <-timeout.C:
		return ErrSendTimeout
	default: // Means client's input was closed.
		return ErrSocketClosed
	}
	return nil
}

func (wsc *WebSocketClient) Conn() *websocket.Conn {
	return wsc.connection
}

func (wsc *WebSocketClient) Channels() []*Channel {
	return wsc.channels
}
