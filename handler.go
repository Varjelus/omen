package omen

import (
	"errors"
)

var (
	ErrNotOpen = errors.New("connection is not open")
)

func Handle(conn Conn) error {
	// Ensure the connection is open.
	if conn.State() != OPEN {
		return ErrNotOpen
	}

	// Reader
	go func() {
		var output []byte
		for conn.State() == OPEN {
			_, err := conn.Read(output)
			if err != nil {
				break
			}
		}
	}()
}
