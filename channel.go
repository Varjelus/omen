package omen

import (
	"golang.org/x/net/websocket"
	"sync"
	"log"
	"io"
)

type subscription chan []byte

type ChannelAuthenticator func(Fingerprint) error

type Channel struct {
	name string
	subscriptions map[Fingerprint]*subscription
	log *log.Logger
	auther ChannelAuthenticator
	mutex *sync.Mutex
}

func NewChannel(name string, auther ChannelAuthenticator, log io.Writer) *Channel {
	ch := &Channel{
		name: name,
		subscriptions: make(map[Fingerprint]*subscription),
		auther: auther,
		mutex: new(sync.Mutex),
	}
	if log != nil {
		ch.log = log.New(log, "[CHANNEL] "+name+"\t", log.LstdFlags)
	}
	return ch
}

func (ch *Channel) Broadcast(data []byte) {
	ch.mutex.Lock()

	var (
		wg *sync.WaitGroup
		closed = make(chan Fingerprint, len(ch.subscriptions))
	)
	for id, sub := range ch.subscriptions {
		wg.Add(1)
		go func() {
			defer wg.Done()
			select {
			case sub <- data:
			default: // Closed
				closed <- id
			}
		}
	}
	wg.Wait()

	if ch.log != nil {
		ch.log.Printf("broadcast %d bytes for %d clients\n", len(data), len(ch.subscriptions))
	}

	// Clean up
	close(closed)
	for id := range closed {
		delete(ch.subscriptions, id)
	}

	ch.mutex.Unlock(s)
}

func (ch *Channel) Subscribe(id Fingerprint) (*subscription, error) {
	if ch.auther != nil {
		if err := ch.auther(id); err != nil {
			return nil, err
		}
	}

	sub := make(subscription)
	ch.mutex.Lock()
	ch.subscriptions[id] = &sub
	ch.mutex.Unlock(s)
	return &sub, nil
}

func (ch *Channel) Unsubscribe(id Fingerprint) {
	ch.mutex.Lock()
	if sub, exist := ch.subscriptions[id]; exist {
		close(sub)
	}
	ch.mutex.Unlock(s)
}
